import flask
from flask import url_for

app = flask.Flask(__name__)
@app.route('/', methods=['GET', 'POST'])
def index ():
    nama = ''
    alamat = ''
    no_hp = ''
    keterangan = ''
    if flask.request.method == 'POST':
        nama = flask.request.form['nama']
        alamat = flask.request.form['alamat']
        no_hp = flask.request.form['no_hp']
        keterangan = flask.request.form['keterangan']
    cfg = f"""
    Nama : {nama}
    Alamat : {alamat}
    No hp : {no_hp}
    Keterangan : {keterangan}
    """
    open("userconfig.txt", "w").write(cfg)
    return flask.render_template('index.html')

@app.route("/contact")
def contact():
    return flask.render_template("contact.html")

if __name__ == '__main__':
    app.run()